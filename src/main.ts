import "./style.css";
import { RouteData } from "./routedata/RouteData";
import SVG from "./view/SVG";
import { OctomapGrid, Options } from "./viz/OctomapGrid";
import { OctoSimple } from "./viz/OctoSimple";

let svg = new SVG("viz1", 4, 3000);

let firstRun = true;

async function main(options: any = {}) {
  console.log("SVG size is ", svg.getWidth(), "x", svg.getHeight());

  const dataset = "atb";
  let routeData = new RouteData(dataset);
  await routeData.load();

  let routes = routeData.getRouteNames();
  console.log("Routes", routes);

  // Function to update the dropdown
  function updateDropdown() {
    const dropdown = document.getElementById("routes");
    if (!dropdown) return;
    dropdown.innerHTML = "";

    // Add new options from the route array
    routes.forEach((routeVal) => {
      // Create a new option element
      const option = document.createElement("option");

      // Set the value and text for the option
      option.value = routeVal;
      option.textContent = "route " + routeVal;

      // Append the option to the dropdown
      dropdown.appendChild(option);
    });
  }

  // Call the function to update the dropdown
  if (firstRun) updateDropdown();

  if (options.filterRoutes) {
    routeData.links = routeData.links.filter((d) =>
      options.filterRoutes?.includes(d.route_short_name)
    );
  }

  if (options.type === "geo") {
    let geomap = new GeoMap(svg, routeData, options);
    geomap.init();
    geomap.update();
  } else if (options.type === "simple") {
    let geomap = new OctoSimple(svg, routeData, options);
    geomap.init();
    geomap.update();
  } else {
    let octomap2 = new OctomapGrid(svg, routeData, options);
    await octomap2.init();
    octomap2.update();
  }

  svg.scrollToCenter();
  firstRun = false;
}

// Set default start config
main({ type: "simple" });

document?.getElementById("btnGeo")?.addEventListener("click", function () {
  main({ type: "geo" });
});

document?.getElementById("btnOptimize")?.addEventListener("click", function () {
  main({ type: "simple" });
});
document?.getElementById("btn1")?.addEventListener("click", function () {
  main({});
});
document?.getElementById("btn2")?.addEventListener("click", function () {
  main({ simple: true });
});
document?.getElementById("btn3")?.addEventListener("click", function () {
  main({ simple: true, filterRoutes: [2] });
});
document
  ?.getElementById("btnSelection")
  ?.addEventListener("click", function () {
    let selection = document.getElementById("routes") as HTMLSelectElement;
    // Initialize an array to hold the selected values
    let selectedValues = [];

    // Iterate over all options
    for (let i = 0; i < selection.options.length; i++) {
      // Check if the option is selected
      if (selection.options[i].selected) {
        // Add the option's value to the array
        selectedValues.push(parseInt(selection.options[i].value, 10));
      }
    }

    console.log("SELECTION IS", selectedValues);
    main({ simple: true, filterRoutes: selectedValues });
  });
