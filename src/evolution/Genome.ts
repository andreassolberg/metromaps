import { GridPath } from "../grid/GridPath";
import { GridPoint } from "../grid/GridPoint";
import { StopInfo } from "../routedata/StopInfo";
import { StopPairMap } from "../routedata/StopPairMap";

export class Genome {
  stops: Map<StopInfo, GridPoint>;
  links: StopPairMap;
  linkByPos: Map<string, GridPath[]>;

  constructor() {
    this.stops = new Map<StopInfo, GridPoint>();
    this.links = new StopPairMap();
    this.linkByPos = new Map<string, GridPath[]>();
  }

  private getLinkPaths(point: GridPoint): GridPath[] {
    // Directly return the value if it exists, or an empty array otherwise.
    return this.linkByPos.get(point.toString()) || [];
  }

  private addLinkByPos(point: GridPoint, linkpath: GridPath) {
    if (!this.linkByPos.has(point.toString())) {
      this.linkByPos.set(point.toString(), [linkpath]);
    } else {
      let was = this.linkByPos.get(point.toString());
      if (was) {
        was.push(linkpath);
        this.linkByPos.set(point.toString(), was);
      }
    }
  }

  update() {
    this.linkByPos.clear();
    for (let linkpath of this.links.values()) {
      this.addLinkByPos(linkpath.first(), linkpath);
      this.addLinkByPos(linkpath.last(), linkpath);
    }
  }

  mutate(stop: StopInfo) {
    console.log("Mutate", this.linkByPos);
    let point = this.stops.get(stop) as GridPoint;

    console.log("lookup", point);
    let linkpaths = this.getLinkPaths(point);
    console.log("linkpaths", linkpaths);
    for (let linkpath of linkpaths) {
      linkpath.movePoint(point, new GridPoint(point.x, point.y - 1));
    }
    point.y -= 1;
  }

  getStopPos(stop: StopInfo): GridPoint {
    let pos = this.stops.get(stop);
    if (!pos)
      throw new Error(
        "Could not get position from genome for stop [" +
          stop.stop_id +
          ", " +
          stop.stop_name +
          "]"
      );
    return pos;
  }

  getLinkPos(links: [StopInfo, StopInfo]): GridPath {
    let path = this.links.get(links);
    if (!path)
      throw new Error(
        "Could not get path from genome between stops [" +
          links[0].stop_name +
          ", " +
          links[1].stop_name +
          "]"
      );
    return path;
  }
}
