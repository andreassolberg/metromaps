import * as d3 from "d3";
import SVG from "./SVG";
import { GridPoint } from "../grid/GridPoint";
import { ViewPos } from "./ViewPos";
import { StopInfo } from "../routedata/StopInfo";
import { StopMarker } from "./StopMarker";
import { GridLink } from "../grid/GridLink";
import { LinkMarker } from "./LinkMarker";
import { Splits } from "../Splits";
import { GridPath } from "../grid/GridPath";
import { ViewPath } from "./ViewPath";
import { GridLabel } from "../grid/GridLabel";
import { RouteLabelMarker } from "./RouteLabelMarker";
import { LabelSpaceFinder } from "../grid/LabelSpaceFinder";
import { StopLabelMarker } from "./StopLabelMarker";
import { Genome } from "../evolution/Genome";
import { AggregatedLink } from "../routedata/AggregatedLink";

export class GridView {
  svg: SVG;
  scaleX: any;
  scaleY: any;
  labelSpaceFinder: LabelSpaceFinder | null = null;
  labelPlacement: Map<string, any> | null = null;

  constructor(svg: SVG) {
    this.svg = svg;
  }

  setupScales(splits: Splits) {
    // console.log("#SETUPSCALES OctomapGrid", this);
    const geoxExtent: [number, number] = splits.getXExtent();
    const geoyExtent: [number, number] = splits.getYExtent();

    // console.log("geo X Extent", geoxExtent);
    // console.log("geo Y Extent", geoyExtent);

    let gridAspectRatio =
      (geoxExtent[1] - geoxExtent[0]) / (geoyExtent[1] - geoyExtent[0]);
    let svgAspectRatio = this.svg.width / this.svg.height;

    // console.log("Aspect ratio grid", gridAspectRatio);
    // console.log("Aspect ratio svg", svgAspectRatio);

    if (gridAspectRatio > svgAspectRatio) {
      const ybuffer = (this.svg.height - this.svg.width / gridAspectRatio) / 2;
      this.scaleX = d3
        .scaleLinear()
        .domain(geoxExtent)
        .range([this.svg.getLeft(), this.svg.getRight()]);

      this.scaleY = d3
        .scaleLinear()
        .domain(geoyExtent)
        .range([this.svg.getBottom() - ybuffer, this.svg.getTop() + ybuffer]);
    } else {
      // console.log("Grid is taller than svg");
      const xbuffer = (this.svg.width - this.svg.height * gridAspectRatio) / 2;
      this.scaleX = d3
        .scaleLinear()
        .domain(geoxExtent)
        .range([this.svg.getLeft() + xbuffer, this.svg.getRight() - xbuffer]);

      this.scaleY = d3
        .scaleLinear()
        .domain(geoyExtent)
        .range([this.svg.getBottom(), this.svg.getTop()]);
    }
  }

  getGridPos(viewPos: ViewPos, round: boolean = true): GridPoint {
    let x = this.scaleX.invert(viewPos.x);
    let y = this.scaleY.invert(viewPos.y);
    if (round) {
      x = Math.round(x);
      y = Math.round(y);
    }
    return new GridPoint(x, y);
  }

  getViewPos(gridPos: GridPoint): ViewPos {
    return new ViewPos(this.scaleX(gridPos.x), this.scaleY(gridPos.y));
  }

  scaleGridPos(pos: GridPoint): ViewPos {
    return new ViewPos(this.scaleX(pos.x), this.scaleY(pos.y));
  }
  scaleGridPath(gridPath: GridPath): ViewPath {
    if (!gridPath.gridPoints) throw new Error("gridPath has no gridPoints");
    let path: ViewPos[] = gridPath.gridPoints.map((d) => this.scaleGridPos(d));
    return new ViewPath(path);
  }

  // Should be deprecated, and instrad get from genome.
  getStopMarkers(stops: StopInfo[]) {
    let markers: StopMarker[] = [];
    for (let stop of stops) {
      markers.push(new StopMarker(stop, this.scaleGridPos(stop.gridPos)));
    }
    return markers;
  }

  getStopMarkersFromGenome(stops: StopInfo[], genome: Genome) {
    let markers: StopMarker[] = [];
    for (let stop of stops) {
      markers.push(
        new StopMarker(stop, this.scaleGridPos(genome.getStopPos(stop)))
      );
    }
    return markers;
  }

  getStopLabelMarkers(stops: StopInfo[], routeData, splits): StopLabelMarker[] {
    let markers: StopLabelMarker[] = [];
    // console.log("--------- Stop label makers MAKING.. ---------");

    let labelSpaceFinder = new LabelSpaceFinder(routeData, splits, 2, this.svg);
    // let labelconfig = labelSpaceFinder.findLabelSpace(stop, true);

    let labelPlacement = new Map();
    for (let stop of stops) {
      labelPlacement.set(stop.stop_id, labelSpaceFinder.findLabelSpace(stop));
    }
    // console.log("labelPlacement", labelPlacement);
    // console.log("Placemebnt", labelSpaceFinder.labelsPlaced);

    for (let stop of stops) {
      if (!labelPlacement.has(stop.stop_id))
        throw new Error("cannot find label placement for ", stop.stop_id);

      let labelSpace = labelPlacement.get(stop.stop_id);
      // console.log("Found labelspace", labelSpace);
      let angle = (labelSpace.angle * 180) / Math.PI;
      let xOffset = angle > 90 && angle < 270 ? -5 : 5;

      // let pos = OctomapGrid.labelOffsetX(labelPlacement, d);
      let pos = this.scaleGridPos(stop.gridPos);
      let textPos = this.scaleGridPos(stop.gridPos);
      textPos.x += xOffset;
      textPos.y += 1;
      let textAngle = angle;
      if (textAngle > 90 && textAngle < 270)
        textAngle = (textAngle + 180) % 360;
      let anchor = angle > 90 && angle < 270 ? "end" : "start";
      let marker = new StopLabelMarker(
        stop.stop_name,
        pos,
        textPos,
        -textAngle,
        anchor
      );
      markers.push(marker);
    }

    return markers;
  }

  getLinkMarkers(gridLinks: GridLink[]): LinkMarker[] {
    let markers: LinkMarker[] = [];
    for (let link of gridLinks) {
      // console.log("link", link);
      if (link.gridPath !== null) {
        let newLinkMarker = new LinkMarker(
          link.aggregatedLink,
          this.scaleGridPath(link.gridPath)
        );
        markers.push(newLinkMarker);
      }
    }
    return markers;
  }

  getLinkMarkersFromGenome(
    aggregatedLinks: AggregatedLink[],
    genome: Genome
  ): LinkMarker[] {
    let markers: LinkMarker[] = [];
    for (let link of aggregatedLinks) {
      let path = genome.getLinkPos([
        link.link.a as StopInfo,
        link.link.b as StopInfo,
      ]);
      // path er GridPath
      // link er AggregatedLink - som har ref til link,index,count,aggregated....
      // Lage en new GridLink(gridLink: GridLink, path: ViewPath)

      if (path) {
        let newLinkMarker = new LinkMarker(link, this.scaleGridPath(path));
        markers.push(newLinkMarker);
      }
    }
    return markers;
  }

  getRoutenameMarkers(gridLabels: GridLabel[]): RouteLabelMarker[] {
    let markers: RouteLabelMarker[] = [];
    for (let label of gridLabels) {
      let newLinkMarker = new RouteLabelMarker(
        label.label,
        this.scaleGridPos(label.pos)
      );
      markers.push(newLinkMarker);
    }
    return markers;
  }
}
