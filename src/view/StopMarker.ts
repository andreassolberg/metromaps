import { StopInfo } from "../routedata/StopInfo";
import { ViewPos } from "./ViewPos";

export class StopMarker {
  stop: StopInfo;
  pos: ViewPos;
  constructor(stop: StopInfo, pos: ViewPos) {
    this.stop = stop;
    this.pos = pos;
  }
}
