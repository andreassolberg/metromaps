import { ViewPos } from "./ViewPos";

export class ViewPath {
  path: ViewPos[] = [];
  constructor(path: ViewPos[]) {
    this.path = path;
  }

  getD() {
    return (
      "M " +
      this.path
        .map(function (d) {
          return d.x + " " + d.y;
        })
        .join(" L ") +
      ""
    );
  }
  getDRounded() {
    if (this.path.length < 3) {
      return this.getD();
    }

    let d = "M " + this.path[0].x + " " + this.path[0].y;
    const radius = 4;

    const calculateVector = (p1: ViewPos, p2: ViewPos) =>
      new ViewPos(p2.x - p1.x, p2.y - p1.y);

    for (let i = 1; i < this.path.length - 1; i++) {
      const prevVector = calculateVector(this.path[i - 1], this.path[i]);
      const nextVector = calculateVector(this.path[i], this.path[i + 1]);

      // Normalize vectors
      const prevLength = Math.hypot(prevVector.x, prevVector.y);
      const nextLength = Math.hypot(nextVector.x, nextVector.y);
      const prevUnit = new ViewPos(
        prevVector.x / prevLength,
        prevVector.y / prevLength
      );
      const nextUnit = new ViewPos(
        nextVector.x / nextLength,
        nextVector.y / nextLength
      );

      // Determine the angle between vectors
      const dotProduct = prevUnit.x * nextUnit.x + prevUnit.y * nextUnit.y;
      const clampedValue = Math.min(1, Math.max(-1, dotProduct));
      const angle = Math.acos(clampedValue);

      let angle2 = Math.round(8 * (angle / (2 * Math.PI)));
      let arcRadius = radius;
      // Skip the arc for a straight line
      if (angle2 === 0 || angle === 4 || angle === 8) {
        d += " L " + this.path[i].x + " " + this.path[i].y;
        continue;
      } else if (angle2 === 2) {
        // console.log("Angle is 90 degrees");
        arcRadius = radius * 2;
      } else if (angle2 === 1) {
        arcRadius = radius * 2;
      } else {
        console.error("Angle is unkown", angle2, prevUnit, nextUnit, this.path);
      }

      // Calculate arc sweep (1 for clockwise, 0 for counterclockwise)
      const crossProduct =
        prevVector.x * nextVector.y - prevVector.y * nextVector.x;
      const sweep = crossProduct > 0 ? 1 : 0;

      // Calculate arc start and end points
      const arcStart = new ViewPos(
        this.path[i].x - radius * prevUnit.x,
        this.path[i].y - radius * prevUnit.y
      );
      const arcEnd = new ViewPos(
        this.path[i].x + radius * nextUnit.x,
        this.path[i].y + radius * nextUnit.y
      );

      d += " L " + arcStart.x + " " + arcStart.y;
      d +=
        " A " +
        arcRadius +
        " " +
        arcRadius +
        " 0 0 " +
        sweep +
        " " +
        arcEnd.x +
        " " +
        arcEnd.y;
    }

    // Line to the last point
    d +=
      " L " +
      this.path[this.path.length - 1].x +
      " " +
      this.path[this.path.length - 1].y;

    return d;
  }
}
