import { ViewPos } from "./ViewPos";

export class RouteLabelMarker {
  label: string;
  pos: ViewPos;
  constructor(label: string, pos: ViewPos) {
    this.label = label;
    this.pos = pos;
  }
}
