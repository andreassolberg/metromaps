import { ViewPos } from "./ViewPos";

export class StopLabelMarker {
  label: string;
  pos: ViewPos;
  textPos: ViewPos;
  angle: number;
  anchor: string;

  constructor(
    label: string,
    pos: ViewPos,
    textPos: ViewPos,
    angle: number,
    anchor: string | "start"
  ) {
    this.label = label;
    this.pos = pos;
    this.textPos = textPos;
    this.angle = angle;
    this.anchor = anchor;
  }
}
