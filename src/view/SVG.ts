import * as d3 from "d3";

export default class SVG {
  id: string;
  svg: any;
  margin: number;
  width: number;
  height: number;
  zoom: number;
  container: HTMLElement | null;
  onClick: ((x: number, y: number) => void) | null;
  constructor(id: string, zoom: number = 1, width: number = 2000) {
    this.id = id;
    this.margin = 50;
    this.width = width;
    this.zoom = zoom;

    this.onClick = null;

    this.container = document.getElementById("svgContainer");
    if (!this.container)
      throw new Error("No #svgContainer found. Is required.");

    this.height = this.#getBoundingBoxWidthRatio(this.container);

    console.log("Height:", this.height);
    console.log("Width:", this.width);

    this.svg = d3.create("svg");
    const percent = Math.round(100 * this.zoom);
    this.svg
      .attr("id", this.id)
      .attr("width", percent + "%")
      .attr("height", percent + "%")
      .attr("viewBox", [0, 0, this.width, this.height]);
    this.svg.attr("font-family", "sans-serif").attr("font-size", 8);
    this.container.appendChild(this.svg.node());

    this.setupEvents();
  }

  scrollToCenter() {
    if (this.container === null) throw new Error("No container");
    // Calculate the center position
    const node = this.svg.node(); // ! .getBoundingClientRect().height
    const centerX = (node.scrollWidth - this.container.clientWidth) / 2;
    const centerY = (node.scrollHeight - this.container.clientHeight) / 8;

    console.log("Scroll to ", centerX, centerY);

    // Scroll to the center
    this.container.scrollTo(centerX, centerY);
  }

  setupEvents() {
    let whenClick = (event: any) => {
      // console.log("=======================> Mouse down");
      // Extract the x and y coordinates
      const x = event.clientX - this.svg.node()!.getBoundingClientRect().left;
      const y = event.clientY - this.svg.node()!.getBoundingClientRect().top;

      const rx =
        (this.width * x) / this.svg.node()!.getBoundingClientRect().width;
      const ry =
        (this.height * y) / this.svg.node()!.getBoundingClientRect().height;

      if (this.onClick !== null) {
        this.onClick(rx, ry);
      }
    };

    let clickbound = whenClick.bind(this);
    document.addEventListener("DOMContentLoaded", (_) => {
      this.svg.on("mousedown", clickbound);
    });
  }

  getTop(): number {
    return this.margin;
  }
  getBottom(): number {
    return this.height - this.margin;
  }
  getLeft(): number {
    return this.margin;
  }
  getRight(): number {
    return this.width - this.margin;
  }

  #getBoundingBoxWidthRatio(container: HTMLElement): number {
    return (
      this.width *
      (container.getBoundingClientRect().height /
        container.getBoundingClientRect().width)
    );
  }

  getWidth(): number {
    return this.svg.node()!.getBoundingClientRect().width;
  }

  getHeight(): number {
    // Return the height of the SVG element
    return this.svg.node()!.getBoundingClientRect().height;
  }
}
