import { AggregatedLink } from "../routedata/AggregatedLink";
import { ViewPath } from "./ViewPath";

export class LinkMarker {
  aggregatedLink: AggregatedLink;
  path: ViewPath;
  constructor(aggregatedLink: AggregatedLink, path: ViewPath) {
    this.aggregatedLink = aggregatedLink;
    this.path = path;
  }
}
