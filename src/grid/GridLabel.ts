import { GridPoint } from "./GridPoint";

export class GridLabel {
  label: string;
  pos: GridPoint;
  constructor(label: string, pos: GridPoint) {
    this.label = label;
    this.pos = pos;
  }
}
