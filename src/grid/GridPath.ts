import { ShapeInfo } from "../routedata/ShapeInfo";
import { GridLine } from "./GridLine";
import { GridPoint } from "./GridPoint";

export class GridPath {
  shape: ShapeInfo | null = null;
  gridPoints: GridPoint[] | null = null;
  constructor() {}

  first(): GridPoint | null {
    return this.gridPoints ? this.gridPoints[0] : null;
  }
  last(): GridPoint | null {
    return this.gridPoints ? this.gridPoints[this.gridPoints.length - 1] : null;
  }

  static closestTo(point: GridPoint, points: GridPoint[]): number {
    let bestIndex = -1;
    let bestDistance = 9999999999;

    for (let i = 0; i < points.length; i++) {
      let distance = points[i].chebyshevDistance(point);
      console.log(
        "Distance to ",
        JSON.stringify(point),
        "Index",
        i,
        "Distance",
        distance,
        "Best",
        JSON.stringify(points[i])
      );
      if (distance <= bestDistance) {
        bestDistance = distance;
        bestIndex = i;
      }
    }
    return bestIndex;
  }

  movePoint(point: GridPoint, newPoint: GridPoint): boolean {
    console.log("Move point from ", point, "to", newPoint);
    console.log("Gridpoints before ", JSON.stringify(this.gridPoints));
    if (!this.gridPoints) return false;
    let reverse: boolean = false;
    let points = this.gridPoints;
    let newPoints: GridPoint[] = [];
    if (point.equals(this.first() as GridPoint)) {
      reverse = false;
    } else if (point.equals(this.last() as GridPoint)) {
      reverse = true;
      points.reverse();
    } else {
      return false;
    }

    let nearestIndex = GridPath.closestTo(newPoint, points);
    if (nearestIndex === -1) return false;

    newPoints.push(newPoint);
    for (let i = nearestIndex; i < points.length; i++) {
      newPoints.push(points[i]);
    }

    for (let i = 0; i < this.gridPoints.length; i++) {
      if (this.gridPoints[i].equals(point)) {
        this.gridPoints[i] = newPoint;
      }
    }

    if (reverse) {
      newPoints.reverse();
    }

    this.gridPoints = newPoints;
    console.log("Gridpoints after ", JSON.stringify(this.gridPoints));
    return true;
  }

  compress() {
    if (!this.gridPoints) return;
    if (this.gridPoints.length < 2) return;
    let compressed: GridPoint[] = [];
    compressed.push(this.gridPoints[0]);
    for (let i = 1; i < this.gridPoints.length; i++) {
      if (this.gridPoints[i].equals(compressed[compressed.length - 1])) {
        continue;
      }
      compressed.push(this.gridPoints[i]);
    }
    this.gridPoints = compressed;
  }

  getSize() {
    if (!this.gridPoints) return 0;
    return this.gridPoints.length - 1;
  }

  hasPoint(point: GridPoint) {
    if (!this.gridPoints) return false;
    for (let i = 0; i < this.gridPoints.length; i++) {
      if (this.gridPoints[i].equals(point)) return true;
    }
    return false;
  }

  getSegment(no: number) {
    if (!this.gridPoints)
      throw new Error("Cannot get segment of empty gridpath");
    if (no < 0 || no > this.gridPoints.length - 2)
      throw new Error(
        "Cannot get segment " +
          no +
          " of gridpath with " +
          this.gridPoints.length +
          " points"
      );
    return new GridLine(this.gridPoints[no], this.gridPoints[no + 1]);
  }

  fillPath() {
    if (!this.gridPoints) return;
    if (this.gridPoints.length < 2) return;

    let newPath: GridPoint[] = [];
    newPath.push(this.gridPoints[0]);

    for (let i = 1; i < this.gridPoints.length; i++) {
      if (
        newPath[newPath.length - 1].chebyshevDistance(this.gridPoints[i]) > 1
      ) {
        let interpolatedPoints = newPath[newPath.length - 1].interpolateLine(
          this.gridPoints[i]
        );
        newPath.push(...interpolatedPoints);
      } else {
        newPath.push(this.gridPoints[i]);
      }
    }
    this.gridPoints = newPath;
  }

  simplify(debugText?: string): boolean {
    if (!this.gridPoints) return false;
    if (this.gridPoints.length < 3) return false;
    // console.log(JSON.stringify(this.gridPoints));
    // console.log("======> " + debugText);
    let fixes = false;
    for (let i = 0; i < this.gridPoints.length; i++) {
      for (let j = this.gridPoints.length - 1; j >= i + 2; j--) {
        // console.log(
        //   " Comparing " + i,
        //   this.gridPoints[i],
        //   " and " + j,
        //   this.gridPoints[j]
        // );
        // console.log(
        //   "this gridpoint",
        //   this.gridPoints,
        //   i,
        //   j,
        //   this.gridPoints.length
        // );
        if (this.gridPoints[i].chebyshevDistance(this.gridPoints[j]) === 1) {
          this.gridPoints.splice(i + 1, j - i - 1);
          fixes = true;
          break;
        }
      }
    }
    return fixes;
  }

  // TODO: Refactor with point gemoetry and more
  shiftLine(index: number, tot: number, debug: boolean = false) {
    if (this.gridPoints === null || this.gridPoints.length < 2) {
      console.error("Grid", this.gridPoints);
      throw new Error(
        "Cannot shift line with less than 2 points. Found " + this.getSize()
      );
    }

    let distance = 0.3;
    if (tot === 2) distance = distance / 2;

    let offset = -distance / 2 + (index / (tot - 1)) * distance;

    if (debug) console.log("Offset", offset);
    if (debug) console.log("Shift line Points", this.gridPoints, index, tot);

    let angle = this.getAngle(this.gridPoints[0], this.gridPoints[1]);
    this.gridPoints[0].offset(angle, offset);

    for (let i = 1; i < this.gridPoints.length - 1; i++) {
      angle = this.getAngle(this.gridPoints[i - 1], this.gridPoints[i + 1]);
      this.gridPoints[i].offset(angle, offset);
      // newpoints.push(this.getOffset(points[i], angle, offset));
    }
    this.gridPoints[this.gridPoints.length - 1].offset(angle, offset);
    // newpoints.push(this.getOffset(points[points.length - 1], angle, offset));
  }

  getAngle(p1: GridPoint, p2: GridPoint) {
    let dx = p2.x - p1.x;
    let dy = p2.y - p1.y;
    let angle = Math.atan2(dy, dx);
    return angle - Math.PI / 2;
  }

  getPathBetween(p1: GridPoint, p2: GridPoint): GridPath | boolean {
    if (!this.gridPoints) return false;

    const a1 = GridPath.findPathPart(this.gridPoints, p1, "forward");
    if (a1) {
      const a2 = GridPath.findPathPart(a1, p2, "backward");
      if (a2) {
        this.gridPoints = a2;
        return true;
      }
    }

    const a3 = GridPath.findPathPart(this.gridPoints.reverse(), p1, "forward");
    if (a3) {
      const a4 = GridPath.findPathPart(a3, p2, "backward");
      if (a4) {
        this.gridPoints = a4;
        return true;
      }
    }
    return false;
  }

  static findPathPart(
    points: GridPoint[],
    search: GridPoint,
    type: "forward" | "backward"
  ): any[] | null {
    for (let i = 0; i < points.length; i++) {
      const p = points[i];
      if (points[i].equals(search)) {
        if (type === "forward") {
          return points.slice(i);
        } else {
          return points.slice(0, i + 1);
        }
      }
    }
    return null;
  }
}
