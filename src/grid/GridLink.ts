import { AggregatedLink } from "../routedata/AggregatedLink";
import { GridPath } from "./GridPath";
export class GridLink {
  aggregatedLink: AggregatedLink;
  gridPath: GridPath | null = null;
  constructor(aggregatedLink: AggregatedLink) {
    this.aggregatedLink = aggregatedLink;
  }

  getSize(): number {
    return this.gridPath?.getSize() || 0;
  }

  getLabelPosition() {
    let relDiffPos =
      this.aggregatedLink.count === 1
        ? 0
        : this.aggregatedLink.index / (this.aggregatedLink.count - 1) - 0.5;
    let relPos = 0.5 + relDiffPos * 0.4;
    // let pos = console.log("About to get pos", link, relPos);
    let pointNo = Math.floor(this.getSize() / 2);
    let lineSegment = this.gridPath?.getSegment(pointNo);
    if (!lineSegment)
      throw new Error(
        "Cannot get segment " +
          pointNo +
          " of gridpath with " +
          this.gridPath?.gridPoints?.length +
          " points"
      );
    return lineSegment.getScaledEndPoint(relPos);
  }
}
