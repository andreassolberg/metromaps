import { Line } from "../primitives/Line";
import { GridPoint } from "./GridPoint";

export class GridLine extends Line {
  getScaledEndPoint(factor: number) {
    const dx = this.p2.x - this.p1.x;
    const dy = this.p2.y - this.p1.y;
    return new GridPoint(this.p1.x + dx * factor, this.p1.y + dy * factor);
  }

  intersects(other: GridLine) {
    const [x1, y1] = [this.p1.x, this.p1.y];
    const [x2, y2] = [this.p2.x, this.p2.y];
    const [x3, y3] = [other.p1.x, other.p1.y];
    const [x4, y4] = [other.p2.x, other.p2.y];

    const d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

    // If d is zero, lines are parallel
    if (d === 0) {
      // Check if the lines are coincident (overlapping)
      const coincident = (x1 - x3) * (y3 - y4) === (y1 - y3) * (x3 - x4);
      if (coincident) {
        // Check if the lines overlap or if one line starts at the other
        const overlapOrConnect =
          Math.min(x1, x2) <= Math.max(x3, x4) &&
          Math.max(x1, x2) >= Math.min(x3, x4) &&
          Math.min(y1, y2) <= Math.max(y3, y4) &&
          Math.max(y1, y2) >= Math.min(y3, y4);
        return overlapOrConnect;
      }
      return false;
    }

    const t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / d;
    const u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / d;

    return t >= 0 && t <= 1 && u >= 0 && u <= 1;
  }
}
