import { Splits } from "../Splits";
import { Genome } from "../evolution/Genome";
import { GeoPos } from "../routedata/GeoPos";
import { RouteData } from "../routedata/RouteData";
import { GridLink } from "./GridLink";
import { GridPath } from "./GridPath";

export class MapMaker {
  splits: Splits;
  routeData: RouteData;
  minAggregate: number = 4;

  constructor(splits: Splits, routeData: RouteData, minAggregate: number = 4) {
    this.splits = splits;
    this.routeData = routeData;
    this.minAggregate = minAggregate;

    for (let stop of this.routeData.stops) {
      stop.gridPos = this.splits.gridify(stop.loc);
    }
  }

  getInitialGenome(): Genome {
    let links = this.routeData.getAggregatedLinks(2);
    let genome = new Genome();

    for (let stop of this.routeData.stops) {
      let gridPos = this.splits.gridify(stop.loc);
      stop.gridPos = gridPos; // Depreated, should only ontain this in the genome.
      genome.stops.set(stop, gridPos);
    }
    for (let link of links) {
      let x = new GridLink(link);
      genome.links.set(
        [
          this.routeData.getStop(link.link.stop_a),
          this.routeData.getStop(link.link.stop_b),
        ],
        this.getLinkPath(x)
      );
    }
    genome.update();
    return genome;
  }

  getAggregatedLinks(): GridLink[] {
    let aggregatedLinks = this.routeData.getAggregatedLinks(this.minAggregate);
    let gridLinks: GridLink[] = [];
    console.log("AggregatedLinks from routeData ››››", aggregatedLinks);
    for (let alink of aggregatedLinks) {
      let gridLink = new GridLink(alink);
      gridLink.gridPath = this.getLinkPath(gridLink);
      gridLinks.push(gridLink);
    }
    return gridLinks;
  }

  private getShapeBetweenStops(
    p1: GeoPos,
    p2: GeoPos,
    route_short_name: number
  ) {
    const routeShapes = this.routeData.shapes.get(route_short_name);
    // console.error("RouteShapes", routeShapes);
    if (!routeShapes) {
      return null;
    }

    const gridPath = new GridPath();
    gridPath.shape = routeShapes;
    gridPath.gridPoints =
      gridPath.shape?.points.map((d) => this.splits.gridify(d)) || null;

    // console.log("GridPath", gridPath);
    gridPath.compress();
    // console.log("_GridPath", gridPath);
    let found = gridPath.getPathBetween(p1, p2);
    if (!found) {
      return null;
    }
    // console.log("__GridPath", gridPath);
    return gridPath;

    // const shape = routeShapes.findPath(p1, p2, this.splits);
    // // if (!shape) throw new Error(`No shape found between ${p1} and ${p2}`);
    // return shape;
  }

  // TODO: Refactor with point gemoetry and more
  private getLinkPath(gridLink: GridLink): GridPath {
    if (!this.splits) throw new Error("Splits not initialized");

    let linkPath = gridLink.aggregatedLink.getLinkPath();
    let stopA = this.routeData.getStop(linkPath.stop_a);
    let stopB = this.routeData.getStop(linkPath.stop_b);

    let stopX = ["NSR:StopPlace:42314"];
    let debugX = false;
    if (stopX.includes(stopA.stop_id) || stopX.includes(stopB.stop_id)) {
      debugX = true;
    }

    /* Find a shape with lon, lat coordinates that connects the two stops
     */
    let shape: GridPath | null = this.getShapeBetweenStops(
      stopA.gridPos,
      stopB.gridPos,
      linkPath.route_short_name
    );

    // console.log("Get shape between stops", shape);
    if (!shape) {
      let interpolated = new GridPath();
      interpolated.gridPoints = stopA.gridPos.interpolateLine(stopB.gridPos);
      shape = interpolated;
    }

    // console.log("Shape > ", shape);

    shape.fillPath();
    shape.simplify();

    if (debugX) console.log("----  About to shift", gridLink.aggregatedLink);

    if (
      gridLink.aggregatedLink.count > 1 &&
      !gridLink.aggregatedLink.aggregated
    ) {
      if (debugX)
        console.log(
          "About to shiftLine",
          gridLink.aggregatedLink.index,
          gridLink.aggregatedLink.count
        );
      shape.shiftLine(
        gridLink.aggregatedLink.index,
        gridLink.aggregatedLink.count,
        debugX
      );
    }
    if (debugX) console.log(" --------------------");

    return shape;
  }
}
