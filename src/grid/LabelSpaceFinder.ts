import { Splits } from "../Splits";
import { RouteData } from "../routedata/RouteData";
import { StopInfo } from "../routedata/StopInfo";
import { GridLine } from "./GridLine";
import { GridPoint } from "./GridPoint";
import { add, interpolateLine } from "../utils";
import SVG from "../view/SVG";

interface PlacedLabel {
  stop: StopInfo;
  start: GridPoint;
  path: GridLine;
  angle: number;
}

export class LabelSpaceFinder {
  routeData: RouteData;
  splits: Splits;
  length: number;
  config: any[];

  labelsPlaced: PlacedLabel[];
  hiddenSvg: SVGElement;
  stopLocations: Map<string, StopInfo>;

  constructor(
    routeData: RouteData,
    splits: Splits,
    length: number = 3,
    svg: SVG
  ) {
    this.routeData = routeData;
    this.splits = splits;
    this.length = length;

    const angles = 16;
    let options = [0, 1, 15, 9, 8, 7, 2, 14, 6, 10];
    this.labelsPlaced = [];

    this.hiddenSvg = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "svg"
    );
    this.hiddenSvg.style.position = "absolute";
    this.hiddenSvg.style.visibility = "hidden";
    this.hiddenSvg.setAttribute("width", svg.zoom * 100 + "%");
    this.hiddenSvg.setAttribute("height", svg.zoom * 100 + "%");
    document.body.appendChild(this.hiddenSvg);

    this.stopLocations = new Map();
    for (let stop of this.routeData.stops) {
      let locs =
        this.splits.getX(stop.loc.x) + "|" + this.splits.getY(stop.loc.y);
      this.stopLocations.set(locs, stop);
    }

    this.config = [];
    for (let i of options) {
      let angle = (i * 2 * Math.PI) / angles;
      let angle2 = (angle * 180) / Math.PI;
      let dest = this.#getCoordinates(this.length, angle);
      console.log("Step", i, "angle", angle2, angle, "dest", dest);
      let points = interpolateLine([0, 0], dest);
      points.shift();
      this.config.push({
        angle: angle,
        points,
        no: i,
      });
    }
  }

  #textBoundingBox(text: string) {
    // Create a text element
    let textElement = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "text"
    );
    textElement.setAttribute("x", "0");
    textElement.setAttribute("y", "0");
    textElement.style.fontSize = "9px";
    textElement.style.fontFamily = "sans-serif";
    textElement.textContent = text;

    // Add text to the SVG
    this.hiddenSvg.appendChild(textElement);

    // Get bounding box
    return textElement.getBBox();
  }

  findLabelSpace(stop: StopInfo, debug: boolean = false) {
    let best = null;
    let bestScore = -9999;

    // console.log("---- findLabelSpace");
    // let loc: [number, number] = [
    // ];

    let loc = new GridPoint(
      this.splits.getX(stop.loc.x),
      this.splits.getY(stop.loc.y)
    );
    if (debug)
      console.log(
        "Checking point > ",
        stop.stop_name + " ",
        loc.x + "|" + loc.y
      );
    let length = this.#textBoundingBox(stop.stop_name).width;

    for (let i = 0; i < this.config.length; i++) {
      let config = this.config[i];
      let thisScore: number = this.scoreLabelConfig(loc, config, length);
      config.score = thisScore;
      if (debug) console.log("Points ", config);
      if (thisScore > bestScore) {
        best = config;
        bestScore = thisScore;
      }
    }
    if (debug) {
      console.log("Label config", best);
    }

    let path = new GridLine(
      loc,
      this.#getDest(loc, best.angle, 0.2 + length / 24)
    );
    let placedLabel: PlacedLabel = {
      stop: stop,
      start: loc,
      angle: best.angle,
      path,
    };
    // if (this.#findOverlap(placedLabel)) {
    //   console.log("Path overlapping from stop ", stop.stop_name);
    // }
    this.labelsPlaced.push(placedLabel);
    return best;
  }

  scoreLabelConfig(loc: GridPoint, config: any, length: number): number {
    const points = config.points;
    let thisScore = 0;

    let path = new GridLine(
      loc,
      this.#getDest(loc, config.angle, 0.2 + length / 24)
    );

    for (let j = 0; j < points.length; j++) {
      const point = add([loc.x, loc.y], points[j]);
      // console.log("checking point", point);
      let found = this.stopLocations.has(point[0] + "|" + point[1]);

      if (!found) {
        // console.log("Available");
        thisScore = thisScore + 1 - j / 10;
      } else {
        // console.log("Taken");
      }
    }
    if (this.#findOverlap(path)) thisScore -= 2;

    return thisScore;
  }

  #findOverlap(path: GridLine) {
    for (let i = 0; i < this.labelsPlaced.length; i++) {
      if (path.intersects(this.labelsPlaced[i].path)) return true;
    }
    return false;
  }

  #getDest(start: GridPoint, angle: number, length: number): GridPoint {
    let destX = start.x + length * Math.cos(angle);
    let destY = start.y + length * Math.sin(angle);
    return new GridPoint(destX, destY);
  }

  #getCoordinates(d: number, angle: number): [number, number] {
    // Calculate x and y coordinates
    var x = d * Math.cos(angle);
    var y = d * Math.sin(angle);
    console.log("| x", x, "y", y);
    // x = Math.round(x);
    // y = Math.round(y);

    x = x > 0 ? Math.round(x + 0.45) : Math.round(x - 0.45);
    y = y > 0 ? Math.round(y + 0.45) : Math.round(y - 0.45);
    // console.log("> x", x, "y", y);
    // Return the coordinates as an array
    return [x, y];
  }
}
