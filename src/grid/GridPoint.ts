import { Point } from "../primitives/Point";

export class GridPoint extends Point {
  interpolateLine(other: GridPoint): GridPoint[] {
    let points: GridPoint[] = [];

    // Extract coordinates from points
    let [x1, y1] = [this.x, this.y];
    let [x2, y2] = [other.x, other.y];

    // Calculate differences
    let dx = Math.abs(x2 - x1);
    let dy = Math.abs(y2 - y1);

    // Set the direction of incrementation
    let sx = x1 < x2 ? 1 : -1;
    let sy = y1 < y2 ? 1 : -1;

    // Initialize error term more symmetrically
    let err = (dx > dy ? dx : -dy) / 2;
    let e2;

    while (true) {
      points.push(new GridPoint(x1, y1));

      if (x1 === x2 && y1 === y2) {
        break;
      }

      e2 = err;

      if (e2 > -dx) {
        err -= dy;
        x1 += sx;
      }

      if (e2 < dy) {
        err += dx;
        y1 += sy;
      }
    }

    return points;
  }
  offset(angle: number, offset: number): void {
    this.x += Math.cos(angle) * offset;
    this.y += Math.sin(angle) * offset;
  }
}
