export function add(a: [number, number], b: [number, number]) {
  return [a[0] + b[0], a[1] + b[1]];
}

// export function simplifyPath2(path: GridPoint[], debugText?: string) {
//   // console.log("======> " + debugText);
//   for (let i = 0; i < path.length; i++) {
//     for (let j = path.length - 1; j >= i + 2; j--) {
//       // console.log(debugText + " Comparing " + i, path[i], " and " + j, path[j]);
//       const distance = path[i].chebyshevDistance(path[j]);
//       if (distance === 1) {
//         // console.error(debugText + "Removing entry: Before", path);
//         path.splice(i + 1, j - i - 1);
//         // console.log(debugText + " After", path);
//         break;
//       }
//     }
//   }
//   return path;
// }

export function interpolateLine(p1: [number, number], p2: [number, number]) {
  let points = [];

  // Extract coordinates from points
  let [x1, y1] = p1;
  let [x2, y2] = p2;

  // Calculate differences
  let dx = Math.abs(x2 - x1);
  let dy = Math.abs(y2 - y1);

  // Set the direction of incrementation
  let sx = x1 < x2 ? 1 : -1;
  let sy = y1 < y2 ? 1 : -1;

  // Initialize error term more symmetrically
  let err = (dx > dy ? dx : -dy) / 2;
  let e2;

  while (true) {
    points.push([x1, y1]);

    if (x1 === x2 && y1 === y2) {
      break;
    }

    e2 = err;

    if (e2 > -dx) {
      err -= dy;
      x1 += sx;
    }

    if (e2 < dy) {
      err += dx;
      y1 += sy;
    }
  }

  return points;
}

// export function createGridPathFromGridPoints(
//   points: GridPoint[],
//   interpolate: boolean = false
// ): GridPoint[] {
//   let result: GridPoint[] = [];

//   for (let point of points) {
//     if (result.length === 0) {
//       result.push(point);
//     } else if (point.equals(result[result.length - 1])) {
//       continue;
//     } else if (point.chebyshevDistance(result[result.length - 1]) > 1) {
//       if (interpolate) {
//         let interpolated = result[result.length - 1].interpolateLine(point);
//         interpolated.shift();
//         // interpolated.reverse();
//         console.log("add interpolated", interpolated);
//         result.push(...interpolated);
//       } else {
//         result.push(point);
//       }
//     } else {
//       result.push(point);
//     }
//   }
//   return result;
// }
