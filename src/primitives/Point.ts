export class Point {
  x: number;
  y: number;
  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  toString(): string {
    return `${this.x},${this.y}`;
  }

  chebyshevDistance(other: Point) {
    return Math.max(Math.abs(other.x - this.x), Math.abs(other.y - this.y));
  }

  equals(other: Point) {
    return this.x === other.x && this.y === other.y;
  }
}
