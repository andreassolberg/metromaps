import * as d3 from "d3";
import { RouteData } from "./routedata/RouteData";
import { GeoPos } from "./routedata/GeoPos";
import { GridPoint } from "./grid/GridPoint";
import { StopInfo } from "./routedata/StopInfo";
const EPSILON = 0.000001;

export class Splits {
  routeData: RouteData;
  morePoints: GeoPos[] = [];
  x: number[];
  y: number[];
  ix: number[];
  iy: number[];
  constructor(routeData: RouteData) {
    this.routeData = routeData;
    this.x = [];
    this.y = [];
    this.ix = [];
    this.iy = [];
    // this._stops = stops;
  }

  addPoints(points: GeoPos[]) {
    for (let point of points) {
      this.morePoints.push(point);
    }
  }

  gridify(pos: GeoPos): GridPoint {
    let x = this.getX(pos.x);
    let y = this.getY(pos.y);
    return new GridPoint(x, y);
  }

  getAllPoints(): GeoPos[] {
    let points: GeoPos[] = [];
    for (let stop of this.routeData.stops) {
      points.push(stop.loc);
    }
    for (let point of this.morePoints) {
      points.push(point);
    }
    return points;
  }

  getInitial() {
    let xsplits: number[] = [];
    let ysplits: number[] = [];

    let points = this.getAllPoints();
    let __points = this.routeData.stops.map((d: StopInfo) => d.loc);

    let s = points.sort((a, b) => a.x - b.x);
    for (let i = 0; i < s.length - 1; i++) {
      xsplits.push(d3.mean([s[i], s[i + 1]], (d) => d.x) as number);
    }
    s = s.sort((a, b) => a.y - b.y);
    for (let i = 0; i < s.length - 1; i++) {
      ysplits.push(d3.mean([s[i], s[i + 1]], (d) => d.y) as number);
    }

    this.x = xsplits;
    this.y = ysplits;
  }

  getX(input: number) {
    for (let i = 0; i < this.x.length; i++) {
      if (input < this.x[i]) return i;
    }
    return this.x.length;
  }

  getY(input: number) {
    for (let i = 0; i < this.y.length; i++) {
      if (input < this.y[i]) return i;
    }
    return this.y.length;
  }

  getXExtent(): [number, number] {
    return [0, this.x.length];
  }
  getYExtent(): [number, number] {
    return [0, this.y.length];
  }

  calculate() {
    let dx = [];
    let dy = [];
    for (let i = 0; i < this.x.length - 1; i++) {
      dx.push(this.x[i + 1] - this.x[i]);
    }
    for (let i = 0; i < this.y.length - 1; i++) {
      dy.push(this.y[i + 1] - this.y[i]);
    }

    const ix = [...dx]
      .map((value, index) => ({ value, index }))
      .sort((a, b) => a.value - b.value)
      .map((item) => item.index);

    const iy = [...dy]
      .map((value, index) => ({ value, index }))
      .sort((a, b) => a.value - b.value)
      .map((item) => item.index);

    // console.log("ix", ix);
    // console.log("iy", iy);

    // this.dx = dx;
    // this.dy = dy;
    this.ix = ix;
    this.iy = iy;
  }

  checkRow(row: number): boolean {
    let m = new Map();
    for (let stop of this.routeData.stops) {
      let x = this.getX(stop.loc.x);
      if (x === row || x === row + 1) {
        let y = this.getY(stop.loc.y);
        if (m.has(y)) {
          return false;
        }
        m.set(y, 1);
      }
    }
    return true;
  }

  checkCol(col: number): boolean {
    let m = new Map();
    for (let stop of this.routeData.stops) {
      let y = this.getY(stop.loc.y);
      if (y === col || y === col + 1) {
        let x = this.getX(stop.loc.x);
        if (m.has(x)) {
          return false;
        }
        m.set(x, 1);
      }
    }
    return true;
  }
  combineRow(row: number) {
    //console.log("Combine row ", row);
    this.x.splice(row, 1);
    this.calculate();
  }
  combineCol(col: number) {
    //console.log("Combine col ", col);
    this.y.splice(col, 1);
    this.calculate();
  }

  findFirstReducableRow() {
    for (let i = 0; i < this.ix.length; i++) {
      let g = this.checkRow(this.ix[i]);
      if (g) {
        return this.ix[i];
      }
    }
    return null;
  }

  findFirstReducableCol() {
    for (let i = 0; i < this.iy.length; i++) {
      let g = this.checkCol(this.iy[i]);
      if (g) {
        return this.iy[i];
      }
    }
    return null;
  }

  reduceOne() {
    //console.log("ReduceOne");

    let done = false;
    let row = this.findFirstReducableRow();
    if (row !== null) {
      //console.log("Combine row ", row);
      this.combineRow(row);
      done = true;
    }

    let col = this.findFirstReducableCol();
    if (col !== null) {
      //console.log("Combine col", col);
      this.combineCol(col);
      done = true;
    }

    return done;
  }

  reduce() {
    let cont = true;
    while (cont) {
      let p = this.reduceOne();
      if (!p) return true;
    }
  }

  addBoundaries() {
    let xextent = d3.extent(this.routeData.stops, (d) => d.loc.x);
    let yextent = d3.extent(this.routeData.stops, (d) => d.loc.y);

    // console.log("--- before ---");
    // console.log("X Extent", this.getXExtent());
    // console.log("Y Extent", this.getYExtent());

    // console.log("Get lowest X ", this.getX(xextent[0]), xextent[0], this.x);
    // console.log("Get lowest Y ", this.getY(yextent[0]), yextent[0], this.y);
    // console.log("Get highest X ", this.getX(xextent[1]), xextent[1], this.x);
    // console.log("Get highest Y ", this.getY(yextent[1]), yextent[1], this.y);

    // console.log("xextent", xextent);
    // console.log("yextent", yextent);

    this.x.unshift(xextent[0] - EPSILON);
    this.x.push(xextent[1] + EPSILON);
    this.y.unshift(yextent[0] - EPSILON);
    this.y.push(yextent[1] + EPSILON);
    // console.log("--- after ---");

    // console.log("Get lowest X ", this.getX(xextent[0]), xextent[0], this.x);
    // console.log("Get lowest Y ", this.getY(yextent[0]), yextent[0], this.y);
    // console.log("Get highest X ", this.getX(xextent[1]), xextent[1], this.x);
    // console.log("Get highest Y ", this.getY(yextent[1]), yextent[1], this.y);

    // console.log("X Extent", this.getXExtent());
    // console.log("Y Extent", this.getYExtent());
  }
}
