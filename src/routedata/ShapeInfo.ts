import { Splits } from "../Splits";
import { compareAB } from "../utils";
import { GeoPos } from "./GeoPos";

export class ShapeInfo {
  route_short_name: number = 0;
  shape_id: string = "";
  points: GeoPos[] = [];

  constructor() {}

  static load(json: {
    shape_id: string;
    route_short_name: number;
    points: [];
  }) {
    const shape = new ShapeInfo();
    shape.route_short_name = json.route_short_name;
    shape.shape_id = json.shape_id;
    shape.points = json.points.map(
      (d: any) => new GeoPos(d[1] as number, d[0] as number)
    );
    return shape;
  }
}
