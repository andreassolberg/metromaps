import { StopInfo } from "./StopInfo";

export class LinkInfo {
  route_short_name: number;
  quay_a: string;
  quay_b: string;
  dist: number;
  dur: number;
  stop_a: string;
  stop_b: string;
  a?: StopInfo;
  b?: StopInfo;

  constructor() {
    this.route_short_name = 0;
    this.quay_a = "";
    this.quay_b = "";
    this.dist = 0;
    this.dur = 0;
    this.stop_a = "";
    this.stop_b = "";
  }

  static load(json: {
    route_short_name: number;
    quay_a: string;
    quay_b: string;
    dist: number;
    dur: number;
    stop_a: string;
    stop_b: string;
  }) {
    const route = new LinkInfo();
    route.route_short_name = json.route_short_name;
    route.quay_a = json.quay_a;
    route.quay_b = json.quay_b;
    route.dist = json.dist;
    route.dur = json.dur;
    route.stop_a = json.stop_a;
    route.stop_b = json.stop_b;
    return route;
  }
}
