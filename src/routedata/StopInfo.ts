import { GridPoint } from "../grid/GridPoint";
import { GeoPos } from "./GeoPos";

export class StopInfo {
  stop_id: string;
  stop_name: string;
  stop_desc: string | null;
  stop_vehicle_type: number;
  loc: GeoPos;
  gridPos: GridPoint;

  constructor() {
    this.stop_id = "";
    this.stop_name = "";
    this.stop_desc = null;
    this.stop_vehicle_type = 0;
    this.loc = new GeoPos(0, 0);
    this.gridPos = new GridPoint(0, 0);
  }

  static load(json: {
    stop_id: string;
    stop_name: string;
    stop_lat: number;
    stop_lon: number;
    stop_desc: string | null;
    stop_vehicle_type: number;
  }) {
    const stop = new StopInfo();
    stop.stop_id = json.stop_id;
    stop.stop_name = json.stop_name;
    stop.stop_desc = json.stop_desc;
    stop.stop_vehicle_type = json.stop_vehicle_type;
    stop.loc = new GeoPos(json.stop_lon, json.stop_lat);
    return stop;
  }
}
