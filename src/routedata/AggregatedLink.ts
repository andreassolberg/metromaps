import { LinkInfo } from "./LinkInfo";

export class AggregatedLink {
  link: LinkInfo;
  linkpath: LinkInfo | null = null;
  index: number;
  count: number;
  aggregated: boolean;
  constructor(
    link: LinkInfo,
    count: number,
    index: number,
    aggregated: boolean = false,
    linkpath: LinkInfo | null = null
  ) {
    this.link = link;
    this.count = count;
    this.index = index;
    this.aggregated = aggregated;
    this.linkpath = linkpath;
  }

  getLinkPath(): LinkInfo {
    if (this.linkpath !== null) return this.linkpath;
    return this.link;
  }
}
