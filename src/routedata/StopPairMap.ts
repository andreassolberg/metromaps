import { StopInfo } from "./StopInfo";

export class StopPairMap {
  private map: Map<string, any>;

  constructor() {
    this.map = new Map<string, any>();
  }

  private _sortAndStringify(keyTuple: [StopInfo, StopInfo]): string {
    const sortedKeys = [keyTuple[0].stop_id, keyTuple[1].stop_id].sort();
    return JSON.stringify(sortedKeys);
  }

  values(): IterableIterator<any> {
    return this.map.values();
  }

  set(keyTuple: [StopInfo, StopInfo], value: any): void {
    const key = this._sortAndStringify(keyTuple);
    this.map.set(key, value);
  }

  get(keyTuple: [StopInfo, StopInfo]): any {
    const key = this._sortAndStringify(keyTuple);
    return this.map.get(key);
  }

  has(keyTuple: [StopInfo, StopInfo]): boolean {
    const key = this._sortAndStringify(keyTuple);
    return this.map.has(key);
  }

  delete(keyTuple: [StopInfo, StopInfo]): boolean {
    const key = this._sortAndStringify(keyTuple);
    return this.map.delete(key);
  }
}
