import * as d3 from "d3";
import { StopInfo } from "./StopInfo";
import { LinkInfo } from "./LinkInfo";
import { ShapeInfo } from "./ShapeInfo";
// import { Splits } from "../Splits";
import { AggregatedLink } from "./AggregatedLink";
// import { GeoPos } from "./GeoPos";

interface AggregatedLinksMemory {
  links: Map<number, LinkInfo[]>;
  key: string;
}

// export interface LinkMarkData {
//   link: LinkInfo;
//   linkpath: LinkInfo;
//   count: number;
//   index: number;
//   tot: number;
//   points?: any[];
// }

export class RouteData {
  dataset: string;
  stops: any[];
  stopmap: Map<string, StopInfo>;
  shapes: Map<number, ShapeInfo>;
  links: any[];
  constructor(dataset: string) {
    this.dataset = dataset;
    this.stops = [];
    this.stopmap = new Map();
    this.links = [];
    this.shapes = new Map();
  }

  getAggregatedLinks(minAggregate: number = 4): AggregatedLink[] {
    // First aggregate links by stop pair, and then by route
    let linkMap: Map<string, AggregatedLinksMemory> = new Map();
    for (let link of this.links) {
      const linkKey = this.getLinkKey(link);
      let status: AggregatedLinksMemory = {
        key: linkKey,
        links: new Map(),
      };
      if (linkMap.has(linkKey)) {
        status = linkMap.get(linkKey) as AggregatedLinksMemory;
      }
      if (status.links.has(link.route_short_name)) {
        status.links.get(link.route_short_name)?.push(link);
      } else {
        status.links.set(link.route_short_name, [link]);
      }
      linkMap.set(linkKey, status);
    }
    // console.log("linkMap >>>>", linkMap);
    let aggregatedLinks: AggregatedLink[] = [];
    for (let [_, link] of linkMap) {
      // console.log("Link ", link);

      let refLink = link.links.values().next().value[0];
      if (link.links.size >= minAggregate) {
        // console.log("Link > ", minAggregate, link.links.values().next().value);

        aggregatedLinks.push(
          new AggregatedLink(
            link.links.values().next().value[0],
            link.links.size,
            0,
            true,
            refLink
          )
        );
      } else {
        let i = 0;

        for (let [_, l] of link.links) {
          // console.log("Processing link", link.links, l);
          let thisLink = l.values().next().value;
          // thisLink.points = refLink.points;
          // Parameters new AgreegatedLink():
          // link: LinkInfo,
          // count: number,
          // index: number,
          // aggregated: boolean = false,
          // linkpath: LinkInfo | null = null
          aggregatedLinks.push(
            new AggregatedLink(thisLink, link.links.size, i++, false, refLink)
          );
        }
      }

      // linkMarkData.push({
      //   link: value.links[0],
      //   count: value.links.length,
      // });
    }
    return aggregatedLinks;
  }

  getLinkKey(link: LinkInfo) {
    if (!this.stopmap) throw new Error("No stops loaded");
    const stopnameA = this.stopmap.get(link.stop_a)?.stop_name || "?";
    const stopnameB = this.stopmap.get(link.stop_b)?.stop_name || "?";
    if (stopnameA > stopnameB) {
      return stopnameA + "_" + stopnameB;
    } else {
      return stopnameB + "_" + stopnameA;
    }
  }

  getStop(stop_id: string): StopInfo {
    if (!this.stopmap) throw new Error("No stops loaded");
    const stop = this.stopmap.get(stop_id);
    if (!stop) throw new Error(`Stop ${stop_id} not found`);
    return stop;
  }

  getXExtent(): [number, number] {
    if (!this.stops) throw new Error("No stops loaded");
    return d3.extent(this.stops, (d) => d.loc.x) as [number, number];
  }

  getYExtent(): [number, number] {
    if (!this.stops) throw new Error("No stops loaded");
    return d3.extent(this.stops, (d) => d.loc.y) as [number, number];
  }

  getRouteNames(): string[] {
    if (!this.links) throw new Error("No links loaded");
    return Array.from(
      new Set(this.links.map((d) => String(d.route_short_name)))
    ).sort((a, b) => {
      // Convert elements to integers and compare them
      return parseInt(a, 10) - parseInt(b, 10);
    });
  }

  getShapesList(): ShapeInfo[] {
    if (!this.shapes) throw new Error("No shapes loaded");
    return Array.from(this.shapes.values());
  }

  load() {
    const files = [
      "/data/" + this.dataset + "/simplegrid-stops.json",
      "/data/" + this.dataset + "/simplegrid-links.json",
      "/data/" + this.dataset + "/simplegrid-shapes.json",
    ];
    return Promise.all(files.map((url) => d3.json(url))).then(
      (values: unknown[]) => {
        if (
          !Array.isArray(values[0]) ||
          !Array.isArray(values[1]) ||
          !Array.isArray(values[2])
        )
          throw new Error("Did not get data for routes, links and shapes");

        this.stops = [];
        for (let i = 0; i < values[0].length; i++) {
          const newStop = StopInfo.load(values[0][i]);
          this.stops.push(newStop);
          this.stopmap.set(newStop.stop_id, newStop);
        }
        this.links = [];
        for (let i = 0; i < values[1].length; i++) {
          let newLink = LinkInfo.load(values[1][i]);
          newLink.a = this.stopmap.get(newLink.stop_a);
          newLink.b = this.stopmap.get(newLink.stop_b);
          this.links.push(newLink);
        }
        this.links = this.links.filter((d) => d.stop_a !== d.stop_b);

        for (let i = 0; i < values[2].length; i++) {
          const si = ShapeInfo.load(values[2][i]);
          this.shapes.set(si.route_short_name, si);
        }
      }
    );
  }
}
