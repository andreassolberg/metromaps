import { RouteData } from "../routedata/RouteData";
import { GridLink } from "../grid/GridLink";
import { GridLabel } from "../grid/GridLabel";

export class RoutenameLabelManager {
  routeData: RouteData;
  // splits: Splits;
  links: GridLink[];
  constructor(routeData: RouteData, links: GridLink[]) {
    this.routeData = routeData;
    this.links = links;
  }

  getLabels(): GridLabel[] {
    let routeLabels: GridLabel[] = [];
    for (let link of this.links) {
      let dist = link.getSize();
      // console.log(
      //   "Processing link",
      //   link,
      //   dist,
      //   link.aggregatedLink.aggregated
      // );
      if (!link.aggregatedLink.aggregated && dist >= 2) {
        routeLabels.push(
          new GridLabel(
            String(link.aggregatedLink.link.route_short_name),
            link.getLabelPosition()
          )
        );
      }
    }
    return routeLabels;
  }
}
