import * as d3 from "d3";
import SVG from "../view/SVG";
import { RouteData } from "../routedata/RouteData";
import { LinkInfo } from "../routedata/LinkInfo";
import { StopInfo } from "../routedata/StopInfo";

export class Octomap {
  svg: SVG;
  routeData: RouteData;
  colorScale: d3.ScaleOrdinal<string, string, never>;

  constructor(svg: SVG, routeData: RouteData) {
    this.svg = svg;
    this.routeData = routeData;

    this.colorScale = d3
      .scaleOrdinal<string>()
      .domain(this.routeData.getRouteNames())
      .range(d3.schemeCategory10);
  }

  init() {
    this.setupScales();
  }

  setupScales() {
    const geoxExtent: [number, number] = this.routeData.getXExtent();
    const geoyExtent: [number, number] = this.routeData.getYExtent();

    console.log("Setup scale ", geoxExtent, geoyExtent);

    this.scaleX = d3
      .scaleLinear()
      .domain(geoxExtent)
      .range([this.svg.getLeft(), this.svg.getRight()]);

    this.scaleY = d3
      .scaleLinear()
      .domain(geoyExtent)
      .range([this.svg.getBottom(), this.svg.getTop()]);
  }

  translateX(x: number) {
    return x;
  }

  translateY(y: number) {
    return y;
  }

  update() {
    this.svg.svg.attr("font-family", "sans-serif").attr("font-size", 6);

    let g = this.svg.svg.append("g").attr("class", "octomap");

    g.selectAll("path")
      .data(this.routeData.links)
      .join("path")
      .attr("stroke", (d: any) => this.colorScale(d.route_short_name))
      .attr("stroke-opacity", 1)
      .attr("stroke-width", 1)
      .attr("d", (d: LinkInfo) => this.getLinkPath(d))
      .attr("fill", "none");

    g.selectAll("circle.c1")
      .data(this.routeData.stops)
      .join("circle")
      .attr("class", "c1")
      .attr("cx", (d: StopInfo) => this.scaleX(this.translateX(d.loc.x)))
      .attr("cy", (d: StopInfo) => this.scaleY(this.translateY(d.loc.y)))
      .attr("fill", "white")
      .attr("stroke", "#3399ff")
      .attr("r", 3);

    g.selectAll("text")
      .data(this.routeData.stops)
      .join("text")
      .attr("x", (d: StopInfo) => this.scaleX(this.translateX(d.loc.x)) + 3.5)
      .attr("y", (d: StopInfo) => this.scaleY(this.translateY(d.loc.y)) + 2.0)
      .attr("fill", "steelblue")
      .attr("opacity", 1)
      .text((d: StopInfo) => d.stop_name);
  }

  getLinkPath(link: LinkInfo): any[] {
    return [];
    // TODO: NEed to be fixed. Return any[] points instead of generated path.d
    // -----
    // let stopA = this.routeData.getStop(link.stop_a);
    // let stopB = this.routeData.getStop(link.stop_b);
    // // console.log(
    // //   "Stopa",
    // //   link.stop_a,
    // //   stopA,
    // //   this.scaleX(stopA.geox),
    // //   this.scaleY(stopA.geoy)
    // // );
    // //console.log("Stopb", link.stop_b, stopB);
    // let dx = link.route_short_name % 8;
    // let dy = link.route_short_name % 8;
    // return `M ${dx + this.scaleX(this.translateX(stopA.geox))} ${
    //   dy + this.scaleY(this.translateY(stopA.geoy))
    // } L ${dx + this.scaleX(this.translateX(stopB.geox))} ${
    //   dy + this.scaleY(this.translateY(stopB.geoy))
    // }`;
  }
}
