import * as d3 from "d3";

import SVG from "../view/SVG";
import { Splits } from "../Splits";
import { RouteData } from "../routedata/RouteData";
import { RoutenameLabelManager } from "./RoutenameLabelManager";
import { GridPoint } from "../grid/GridPoint";
import { GridView } from "../view/GridView";
import { MapMaker } from "../grid/MapMaker";
import { StopMarker } from "../view/StopMarker";
import { LinkMarker } from "../view/LinkMarker";
import { RouteLabelMarker } from "../view/RouteLabelMarker";
import { StopLabelMarker } from "../view/StopLabelMarker";
import { GridPath } from "../grid/GridPath";
import { ViewPos } from "../view/ViewPos";
import { GridLink } from "../grid/GridLink";
import { AggregatedLink } from "../routedata/AggregatedLink";

export interface Options {
  simple?: boolean;
  filterRoutes?: number[];
}
export class OctomapGrid {
  svg: SVG;
  routeData: RouteData;
  colorScale: d3.ScaleOrdinal<string, string, never>;

  splits: Splits | null;
  options: Options;
  gridView: GridView | null;
  aggregatedLinks: GridLink[] = [];
  constructor(
    svg: SVG,
    routeData: RouteData,
    options: Options = { simple: false }
  ) {
    this.svg = svg;
    this.routeData = routeData;

    this.colorScale = d3
      .scaleOrdinal<string>()
      .domain(this.routeData.getRouteNames())
      .range(d3.schemeCategory10);

    this.splits = null;
    this.gridView = null;
    this.options = options;
    this.svg.onClick = (x: number, y: number): void => {
      this.onClick(x, y);
    };
  }

  init() {
    this.splits = new Splits(this.routeData);
    this.splits.getInitial();
    this.splits.calculate();
    this.splits.reduce();
    this.splits.addBoundaries();
    // console.log("SPLITS object", this.splits);
  }

  onClick(x: number, y: number) {
    let gpos: GridPoint = this.gridView?.getGridPos(
      new ViewPos(x, y)
    ) as GridPoint;
    if (typeof gpos === undefined) return;
    let viewPos = this.gridView?.getViewPos(gpos);
    if (!viewPos) return;

    console.log("-------- click ---- ", gpos);

    let red = this.svg.svg
      .append("circle")
      .attr("class", "c1")
      .attr("cx", viewPos.x)
      .attr("cy", viewPos.y)
      .attr("stroke", "red")
      .attr("fill", "none")
      .attr("r", 10);

    setTimeout(() => {
      red.remove();
    }, 15000);

    // this.aggregatedLinks = this.mapMaker.getAggregatedLinks();
    const stopMarkers = this.gridView?.getStopMarkers(this.routeData.stops);
    const linkMarkers = this.gridView?.getLinkMarkers(this.aggregatedLinks);
    if (!stopMarkers) return;
    if (!linkMarkers) return;

    let found = stopMarkers.find((d) =>
      d.stop.gridPos.equals(gpos as GridPoint)
    );
    let foundLink = linkMarkers.filter((d: LinkMarker) => {
      const a = this.routeData.getStop(d.gridLink.aggregatedLink.link.stop_a);
      const b = this.routeData.getStop(d.gridLink.aggregatedLink.link.stop_b);
      return (
        a?.gridPos.equals(gpos as GridPoint) ||
        b?.gridPos.equals(gpos as GridPoint)
      );
    });

    const printLink = (d: AggregatedLink, l: LinkMarker) => {
      console.log(l);
      const a = this.routeData.getStop(d.link.stop_a);
      const b = this.routeData.getStop(d.link.stop_b);
      console.log(
        d.index +
          " of " +
          d.count +
          " " +
          d.aggregated +
          " " +
          d.link.route_short_name +
          " " +
          a.stop_name +
          a.stop_id +
          "  " +
          b.stop_name +
          b.stop_id +
          " ... " +
          l.path.getD()
      );
    };
    console.log("stop markers", found);
    foundLink.map((d) => printLink(d.gridLink.aggregatedLink, d));
  }

  async update() {
    if (!this.splits) throw new Error("Splits not initialized");

    const mapMaker = new MapMaker(this.splits, this.routeData);

    this.aggregatedLinks = mapMaker.getAggregatedLinks();
    console.log("Aggregated links !!! ›››", this.aggregatedLinks);

    const gridView = new GridView(this.svg);
    this.gridView = gridView;
    gridView.setupScales(this.splits);
    const stopMarkers = gridView.getStopMarkers(this.routeData.stops);
    const stopLabelMarkers = gridView.getStopLabelMarkers(
      this.routeData.stops,
      this.routeData,
      this.splits
    );
    console.log("--------- Stop label makers ---------");
    console.log(stopLabelMarkers);
    console.log("----------");

    const linkMarkers = gridView.getLinkMarkers(this.aggregatedLinks);
    console.log(stopMarkers);
    console.log(linkMarkers);

    let response = await fetch("data/atb/land-polygon.txt");
    let landraw = await response.text();
    let landpoints = landraw
      .split(",")
      .map((d) => d.trim().split(" ").map(Number));
    let landgridRaw = landpoints.map(
      (d) =>
        new GridPoint(
          this.splits?.getX(d[0]) as number,
          this.splits?.getY(d[1]) as number
        )
    );
    // let landgrid = createGridPathFromGridPoints(landgridraw, false);
    // let landgridSimple = simplifyPath2(landgrid);

    let landGrid = new GridPath();
    landGrid.gridPoints = landgridRaw;
    landGrid.compress();
    // landGrid.fillPath();
    landGrid.simplify();
    let landgridView = gridView.scaleGridPath(landGrid);
    console.log("Land grid  view", landGrid);

    // console.log("Land", landpoints);
    // console.log("Land grid raw", landgridraw);
    // console.log("Land grid", landgrid);
    // console.log("Land grid simple", landgridSimple);

    let routenameLabelManager = new RoutenameLabelManager(
      this.routeData,
      this.aggregatedLinks
    );
    const routenameLabels = routenameLabelManager.getLabels();
    const routenameMakers = gridView.getRoutenameMarkers(routenameLabels);

    // console.log("routemanager", routenameLabelManager);
    // console.log("routenameLabels", routenameLabels);
    // console.log("routenameMakers", routenameMakers);
    let x = this.svg.svg.node();
    while (x.firstChild) {
      x.removeChild(x.firstChild);
    }

    let g = this.svg.svg.append("g").attr("class", "octomap");
    let gLand = g.append("g").attr("class", "land");

    gLand
      .selectAll("path.land")
      .data(this.options.simple ? [] : [landgridView])
      .join("path")
      .attr("class", "land")
      .attr("d", (d: any) => d.getD())
      .attr("fill", "#d0dfff")
      .attr("stroke", "none");

    g.selectAll("path.link")
      .data(linkMarkers)
      .join("path")
      .attr("class", "link")
      .attr("stroke", (d: LinkMarker) =>
        !d.aggregatedLink.aggregated
          ? this.colorScale(String(d.aggregatedLink.link.route_short_name))
          : "#99A"
      )
      .attr("stroke-opacity", (d: LinkMarker) =>
        d.aggregatedLink.aggregated ? 1 : 0.8
      )
      .attr("stroke-width", (d: LinkMarker) =>
        d.aggregatedLink.aggregated ? 4 : 2
      )
      .attr("d", (d: LinkMarker) => d.path.getDRounded())
      .attr("fill", "none");

    g.selectAll("circle.stopmarker")
      .data(stopMarkers)
      .join("circle")
      .attr("class", "stopmarker")
      .attr("cx", (d: StopMarker) => d.pos.x)
      .attr("cy", (d: StopMarker) => d.pos.y)
      .attr("fill", "white")
      .attr("stroke", "#3399ff")
      .attr("r", 4);

    // console.log("Route stops", this.routeData.stops);

    g.selectAll("text.routelabel")
      .data(this.options.simple ? [] : routenameMakers)
      .join("text")
      .attr("class", "routelabel")
      .attr("font-size", "6px")
      .attr("text-anchor", "middle")
      .attr("dominant-baseline", "middle")
      .attr("x", (d: RouteLabelMarker) => d.pos.x)
      .attr("y", (d: RouteLabelMarker) => d.pos.y)
      .attr("fill", "white")
      .attr("paint-order", "stroke")
      .attr("stroke-width", "2px")
      .attr("stroke-linecap", "butt")
      .attr("stroke-linejoin", "miter")
      .attr("stroke", (d: RouteLabelMarker) => this.colorScale(d.label))
      .attr("stroke-opacity", 1)
      .attr("opacity", 0.8)
      .text((d: any) => d.label);

    // let debugText = false;
    g.selectAll("text.stoplabel")
      .data(this.options.simple ? [] : stopLabelMarkers)
      .join("text")
      .attr("class", "stoplabel")
      .attr("text-anchor", (d: StopLabelMarker) => d.anchor)
      .attr("dominant-baseline", "middle")
      .attr("x", (d: StopLabelMarker) => d.textPos.x)
      .attr("y", (d: StopLabelMarker) => d.textPos.y)
      .attr("fill", "steelblue")
      .attr("paint-order", "stroke")
      .attr("stroke-width", "1px")
      .attr("stroke-linecap", "butt")
      .attr("stroke-linejoin", "miter")
      .attr("stroke", "white")
      .attr("stroke-opacity", 0.8)
      .attr("opacity", 1)
      .text((d: StopLabelMarker) => d.label)
      .attr(
        "transform",
        (d: StopLabelMarker) =>
          "rotate(" + d.angle + " " + d.pos.x + " " + d.pos.y + ")"
      );

    // g.selectAll("path.labelpath")
    //   .data(labelSpaceFinder.labelsPlaced)
    //   .join("path")
    //   .attr(
    //     "d",
    //     (d: any) =>
    //       `M ${this.scaleX(d.path.p1.x)}, ${this.scaleY(
    //         d.path.p1.y
    //       )} L ${this.scaleX(d.path.p2.x)}, ${this.scaleY(d.path.p2.y)}`
    //   )
    //   .attr("class", "labelpath")
    //   .attr("stroke", "red")
    //   .attr("stroke-width", 1)
    //   .attr("fill", "none");
  }
}
