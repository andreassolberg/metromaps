import * as d3 from "d3";

import SVG from "../view/SVG";
import { Splits } from "../Splits";
import { RouteData } from "../routedata/RouteData";

import { GridPoint } from "../grid/GridPoint";
import { GridView } from "../view/GridView";
import { MapMaker } from "../grid/MapMaker";
import { StopMarker } from "../view/StopMarker";
import { LinkMarker } from "../view/LinkMarker";

import { ViewPos } from "../view/ViewPos";
import { GridLink } from "../grid/GridLink";
import { AggregatedLink } from "../routedata/AggregatedLink";
import { Genome } from "../evolution/Genome";

export interface Options {
  simple?: boolean;
  filterRoutes?: number[];
}
export class OctoSimple {
  svg: SVG;
  routeData: RouteData;
  colorScale: d3.ScaleOrdinal<string, string, never>;

  genome?: Genome;

  splits: Splits | null;
  options: Options;
  gridView: GridView | null;
  aggregatedLinks: GridLink[] = [];
  constructor(
    svg: SVG,
    routeData: RouteData,
    options: Options = { simple: false }
  ) {
    this.svg = svg;
    this.routeData = routeData;

    this.colorScale = d3
      .scaleOrdinal<string>()
      .domain(this.routeData.getRouteNames())
      .range(d3.schemeCategory10);

    this.splits = null;
    this.gridView = null;
    this.options = options;
    this.svg.onClick = (x: number, y: number): void => {
      this.onClick(x, y);
    };
  }

  init() {
    this.splits = new Splits(this.routeData);
    this.splits.getInitial();
    this.splits.calculate();
    this.splits.reduce();
    this.splits.addBoundaries();
    // console.log("SPLITS object", this.splits);
  }

  onClick(x: number, y: number) {
    let gpos: GridPoint = this.gridView?.getGridPos(
      new ViewPos(x, y)
    ) as GridPoint;
    if (typeof gpos === undefined) return;
    let viewPos = this.gridView?.getViewPos(gpos);
    if (!viewPos) return;
    if (!this.gridView) return;

    let red = this.svg.svg
      .append("circle")
      .attr("class", "c1")
      .attr("cx", viewPos.x)
      .attr("cy", viewPos.y)
      .attr("stroke", "red")
      .attr("fill", "none")
      .attr("r", 10);

    setTimeout(() => {
      red.remove();
    }, 15000);

    let aggregatedLinks2 = this.routeData.getAggregatedLinks(3);
    const stopMarkers = this.gridView.getStopMarkersFromGenome(
      this.routeData.stops,
      this.genome as Genome
    );
    const linkMarkers = this.gridView.getLinkMarkersFromGenome(
      aggregatedLinks2,
      this.genome as Genome
    );

    if (!stopMarkers) return;
    if (!linkMarkers) return;

    let found = stopMarkers.find((d) =>
      d.stop.gridPos.equals(gpos as GridPoint)
    );
    console.log("Found stop", found);
    let foundLink = linkMarkers.filter((d: LinkMarker) => {
      const a = this.routeData.getStop(d.gridLink.aggregatedLink.link.stop_a);
      const b = this.routeData.getStop(d.gridLink.aggregatedLink.link.stop_b);
      return (
        a?.gridPos.equals(gpos as GridPoint) ||
        b?.gridPos.equals(gpos as GridPoint)
      );
    });

    const printLink = (d: AggregatedLink, l: LinkMarker) => {
      console.log(l);
      const a = this.routeData.getStop(d.link.stop_a);
      const b = this.routeData.getStop(d.link.stop_b);
      console.log(
        d.index +
          " of " +
          d.count +
          " " +
          d.aggregated +
          " " +
          d.link.route_short_name +
          " " +
          a.stop_name +
          a.stop_id +
          "  " +
          b.stop_name +
          b.stop_id +
          " ... " +
          l.path.getD()
      );
    };
    console.log("stop markers", found);
    foundLink.map((d) => printLink(d.gridLink.aggregatedLink, d));
  }

  async update() {
    if (!this.splits) throw new Error("Splits not initialized");

    const mapMaker = new MapMaker(this.splits, this.routeData, 2);

    this.genome = mapMaker.getInitialGenome();
    console.log(" .------ Genome -----.");
    console.log(this.genome);

    let stopX = this.routeData.getStop("NSR:StopPlace:42314");
    this.genome.mutate(stopX);

    let aggregatedLinks2 = this.routeData.getAggregatedLinks(3);

    // this.aggregatedLinks = mapMaker.getAggregatedLinks();
    // console.log("Aggregated links !!! ›››", this.aggregatedLinks);

    const gridView = new GridView(this.svg);
    this.gridView = gridView;
    if (!this.gridView) throw new Error("GridView not initialized");
    gridView.setupScales(this.splits);

    // const stopMarkers = gridView.getStopMarkers(this.routeData.stops);
    // const linkMarkers = gridView.getLinkMarkers(this.aggregatedLinks);

    const stopMarkers = this.gridView.getStopMarkersFromGenome(
      this.routeData.stops,
      this.genome
    );
    const linkMarkers = this.gridView.getLinkMarkersFromGenome(
      aggregatedLinks2,
      this.genome
    );

    console.log(stopMarkers);
    console.log(linkMarkers);

    let x = this.svg.svg.node();
    while (x.firstChild) {
      x.removeChild(x.firstChild);
    }

    let g = this.svg.svg.append("g").attr("class", "octomap");

    g.selectAll("path.link")
      .data(linkMarkers)
      .join("path")
      .attr("class", "link")
      .attr("stroke", (d: LinkMarker) =>
        !d.aggregatedLink.aggregated
          ? this.colorScale(String(d.aggregatedLink.link.route_short_name))
          : "#99A"
      )
      .attr("stroke-opacity", 0.6)
      .attr("stroke-width", (d: LinkMarker) =>
        d.aggregatedLink.aggregated ? 2 : 1
      )
      .attr("d", (d: LinkMarker) => d.path.getDRounded())
      .attr("fill", "none");

    g.selectAll("circle.stopmarker")
      .data(stopMarkers)
      .join("circle")
      .attr("class", "stopmarker")
      .attr("cx", (d: StopMarker) => d.pos.x)
      .attr("cy", (d: StopMarker) => d.pos.y)
      .attr("fill", "white")
      .attr("stroke", "#3399ff")
      .attr("r", 3);
  }
}
